#!/bin/bash

NTESTS=10
FAILURES=0
APP=../build/MIA/bin/MIA
REPORT=report.log


if [ -f $REPORT ]; then
	rm $REPORT
fi


for test in "$@"
do

	CMD="$APP test_${test} -compare gold_${test}"
	RES=`$CMD`

	if [ $RES = "0" ]; then
		OUT="FAILURE"
		((FAILURES++))
	else
		OUT="SUCCESS"
	fi

	echo "$CMD $OUT"
	echo "$CMD $OUT" >> $REPORT
done

MSG="$FAILURES failures out of $# tests."
echo
echo $MSG
echo >> $REPORT
echo $MSG >> $REPORT
