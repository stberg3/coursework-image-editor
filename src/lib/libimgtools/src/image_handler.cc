/*******************************************************************************
 * Name            : image_handler.cc
 * Project         : image_tools
 * Module          : io_handler
 * Description     : Implementation of ImageHandler class
 * Copyright       : 2016 CSCI3081W TAs. All rights reserved.
 * Creation Date   : 4/2/15
 * Original Author : Sarit Ghildayal
 *
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "lib/libimgtools/src/include/image_handler.h"
#include <iostream>
#include "lib/libimgtools/src/include/pixel_buffer.h"
#include "lib/libimgtools/src/include/color_data.h"
#include "lib/libimgtools/src/include/i_png_handler.h"
#include "lib/libimgtools/src/include/i_jpeg_handler.h"

/*******************************************************************************
 * Namespaces
 ******************************************************************************/
namespace image_tools {

/*******************************************************************************
 * Static Functions
 ******************************************************************************/
PixelBuffer* ImageHandler::LoadImage(const std::string & file_name) {
  IMAGE_TYPE type = image_type(file_name);
  PixelBuffer* loaded_image = NULL;
  if (type == PNG_IMAGE) {
    loaded_image = IPNGHandler::LoadImage(file_name);
  } else if (type == JPEG_IMAGE) {
    loaded_image = IJPEGHandler::LoadImage(file_name);
  }  else {
    return NULL;
  }
  return loaded_image;
}

bool ImageHandler::CompareImages(const std::string & file_name1,
                                         const std::string & file_name2) {
  IMAGE_TYPE file_type1 =  image_type(file_name1);
  IMAGE_TYPE file_type2 =  image_type(file_name2);

  if (file_type1 != file_type2)
    return false;

  PixelBuffer* loaded_image1 = NULL;
  PixelBuffer* loaded_image2 = NULL;

  loaded_image1 = ImageHandler::LoadImage(file_name1);
  loaded_image2 = ImageHandler::LoadImage(file_name2);

  if (loaded_image1->height() != loaded_image2->height() ||
      loaded_image1->width()  != loaded_image2->width())
      return false;

  for (int i=0; i < loaded_image1->height(); i++) {
    for (int j=0; j < loaded_image1->width(); j++) {
      if (loaded_image1->get_pixel(j, i) != loaded_image2->get_pixel(j, i))
        return false;
    }
  }

  return true;
}

bool ImageHandler::SaveImage(const std::string & file_name,
                             const PixelBuffer* buffer) {
  IMAGE_TYPE type = image_type(file_name);
  bool success = false;
  if (type == PNG_IMAGE) {
    success = IPNGHandler::SaveImage(file_name, buffer);
  } else if (type == JPEG_IMAGE) {
    success = IJPEGHandler::SaveImage(file_name, buffer);
  } else {
    return NULL;
  }
  return success;
}

ImageHandler::IMAGE_TYPE ImageHandler::image_type(
    const std::string & file_name) {
  if (file_name.find(".png", file_name.size()-5) != std::string::npos) {
    return PNG_IMAGE;
  } else if (file_name.find(".jpg", file_name.size()-5) != std::string::npos ||
             file_name.find(".jpeg", file_name.size()-6) != std::string::npos) {
    return JPEG_IMAGE;
  } else {
    return UNKNOWN_IMAGE;
  }
}

} /* namespace image_tools */
