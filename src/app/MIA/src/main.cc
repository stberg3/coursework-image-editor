/*******************************************************************************
 * Name            : main.cc
 * Project         : MIA
 * Module          : main
 * Description     : Entry point for MIA application
 * Copyright       : 2016 CSCI3081W TAs. All rights reserved.
 * Creation Date   : 1/15/15
 * Original Author : Seth Johnson
 *
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/
/* FIXME: ADDITIONAL INCLUDES AS NECESSARY HERE :-) */
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include "lib/libimgtools/src/include/filter.h"
#include "lib/libimgtools/src/include/filter_factory.h"
#include "lib/libimgtools/src/include/filter_manager.h"
#include "lib/libimgtools/src/include/image_handler.h"
#include "app/MIA/src/include/mia_app.h"
#include "lib/libimgtools/src/include/pixel_buffer.h"
#include "lib/libimgtools/src/include/t_stamp.h"
/*******************************************************************************
 * Non-Member Functions
 ******************************************************************************/
void show_help_message(void) {
    std::string help_string =
    "\nusage: MIA <input file name> <command(s)> <output file name>"+
    std::string("\n\n\tCommands:")+
    std::string("\n\t-h                                display this  message")+
    std::string("\n\t-sharpen <float>                  sharpen input image")+
    std::string("\n\t                                  input: [0.0, 100.0]")+
    std::string("\n\t-edge                             apply edge detection")+
    std::string("\n\t-saturate <float>                 adjust color intensity")+
    std::string("\n\t                                  image")+
    std::string("\n\t                                  input: [-10.0, 10.0]")+
    std::string("\n\t-threshold <int>                  apply threshold")+
    std::string("\n\t                                  input: [0.0, 1.0]")+
    std::string("\n\t-blur <float>                     apply gaussian blur")+
    std::string("\n\t                                  input: [0.0, 20.0]")+
    std::string("\n\t-channel <float> <float> <float>  adjust RGB channels")+
    std::string("\n\t                                  input: [0.0, 1.0]")+
    std::string("\n\t<file 1> -compare <file 2>        returns 1 if files")+
    std::string("\n\t                                  are the same, else 0")+
    std::string("\n\t-quantize <int>                   apply threshold")+
    std::string("\n\t                                  input: [2, 256]");

    std::cout << help_string << "\n";
}

int countPound(std::string str) {
  int count = 0;
  int nPos = str.find("#", 0);
  while (nPos != std::string::npos) {
      count++;
      nPos = str.find("#", nPos+1);
  }
  return count;
}

int main(int argc, char** argv) {
  if (argc == 1) {
    image_tools::MIAApp *app = new image_tools::MIAApp( 400,
                                                        400,
                                                        "resources/marker.png");
    app->Init(argc, argv, 50, 50,
              image_tools::ColorData(1, 1, static_cast<float>(0.95)));
    app->RunMainLoop();
    delete app;
  } else {
    static struct option long_options[] = {
         {"help",       0,    0,   'h'      },
         {"sharpen",    1,    0,   's'      },
         {"edge",       0,    0,   'e'      },
         {"threshold",  1,    0,   't'      },
         {"quantize",   1,    0,   'q'      },
         {"blur",       1,    0,   'b'      },
         {"saturate",   1,    0,   'a'      },
         {"channel",    1,    0,   'c'      },
         {"mark",       0,    0,   'm'      },
         {"compare",    0,    0,   'p'      },
        //  {"undo",       0,    0,   'u'      }, TODO maybe we'll need these
        //  {"redo",       0,    0,   'r'      }, for regression tests...
         {NULL,         0,    0,   '\0'     }
      };

      image_tools::FilterManager filter_manager;
      image_tools::MIAIOManager MIAIOManager;
      std::string file_in = argv[1];
      std::string file_out = argv[argc-1];
      image_tools::PixelBuffer * image_buffer =
                                 image_tools::ImageHandler::LoadImage(file_in);
      image_tools::Filter * filter;
      std::string file_iter = file_in;
      std::string file_iterO = file_out;
    int iterations = pow(10, countPound(file_in));
    for (int i = 0; i < iterations; i++) {
      if (iterations > 1) {
      file_iter =
        MIAIOManager.image_name_plus_seq_offset(file_in, i);
      }      
      image_buffer = image_tools::ImageHandler::LoadImage(file_iter);
      if (image_buffer) {
      int opt = 0;
      int option_index = 0;
    while ((opt = getopt_long_only(argc, argv, "", long_options, &option_index))
            != -1) {
      switch (opt) {
        case 'h' :
          show_help_message();
          return 0;
         case 's' :
           if (std::atof(argv[optind-1]) < 0 ||
               100 < std::atof(argv[optind-1])) {
             printf("%s\n", "INVALID sharpen amount");
             continue;
           }
           filter = image_tools::FilterFactory::CreateFilter(
                                     image_tools::FilterFactory::FILTER_SHARPEN,
                                     1,
                                     std::atof(argv[optind-1]));
            image_tools::FilterFactory::ApplyFilter(*filter, &image_buffer);
            break;
         case 'e' :
           filter = image_tools::FilterFactory::CreateFilter(
                                 image_tools::FilterFactory::FILTER_EDGE_DETECT,
                                 1);
           image_tools::FilterFactory::ApplyFilter(*filter, &image_buffer);
           break;
         case 't' :
           if (std::atof(argv[optind-1]) < 0 || 1 < std::atof(argv[optind-1])) {
             printf("%s\n", "INVALID threshold amount");
             continue;
           }
           filter = image_tools::FilterFactory::CreateFilter(
                                   image_tools::FilterFactory::FILTER_THRESHOLD,
                                   1,
                                   std::atof(argv[optind-1]));
           image_tools::FilterFactory::ApplyFilter(*filter, &image_buffer);
           break;
         case 'q':
           if (std::atoi(argv[optind-1]) < 2 ||
                    256 < std::atoi(argv[optind-1])) {
              printf("%s\n", "INVALID number of quantize bins");
              continue;
           }
           filter = image_tools::FilterFactory::CreateFilter(
                                   image_tools::FilterFactory::FILTER_QUANTIZE,
                                                   1,
                                     std::atoi(argv[optind-1]));
             image_tools::FilterFactory::ApplyFilter(*filter, &image_buffer);
             break;
          case 'b':
            if (std::atof(argv[optind-1]) < 0 ||
                20 < std::atof(argv[optind-1])) {
              printf("%s\n", "INVALID blur amount");
              continue;
            }
            filter = image_tools::FilterFactory::CreateFilter(
                                       image_tools::FilterFactory::FILTER_BLUR,
                                       1,
                                       std::atof(argv[optind-1]));
            image_tools::FilterFactory::ApplyFilter(*filter, &image_buffer);
            break;
          case 'a':
            if (std::atof(argv[optind-1]) < -10 ||
                      10 < std::atof(argv[optind-1])) {
              printf("%s\n", "INVALID saturation amount");
              continue;
            }
            filter = image_tools::FilterFactory::CreateFilter(
                                  image_tools::FilterFactory::FILTER_SATURATION,
                                  1,
                                  std::atof(argv[optind-1]));
             image_tools::FilterFactory::ApplyFilter(*filter, &image_buffer);
             break;
           case 'c':
             if (std::atof(argv[optind]) < 0 || 1 < std::atof(argv[optind])   ||
                std::atof(argv[optind+1]) < 0 || 1 < std::atof(argv[optind+1])||
                std::atof(argv[optind+2]) < 0 || 1 < std::atof(argv[optind])) {
               printf("%s\n", "INVALID channel args");
               continue;
             }

             filter = image_tools::FilterFactory::CreateFilter(
                                    image_tools::FilterFactory::FILTER_CHANNELS,
                                    1,
                                    std::atof(argv[optind]),
                                    std::atof(argv[optind+1]),
                                    std::atof(argv[optind+2]));

               image_tools::FilterFactory::ApplyFilter(*filter, &image_buffer);
              break;
            case 'p':
              if (image_tools::ImageHandler::CompareImages(argv[1],
                                                          argv[argc-1])) {
                printf("1\n");
                return 1;
              } else {
                printf("0\n");
                return 0;
              }
              break;
            default:
              show_help_message();
              exit(1);
        }
      }
    if (iterations > 1) {
    file_iterO =
      MIAIOManager.image_name_plus_seq_offset(file_out, i);
    }
    if (!image_tools::ImageHandler::SaveImage(file_iterO, image_buffer)) {
      fprintf(stderr, "%s\n", "Failed to save image");
    }
  }
  }
  }
  return 0;
} /* main() */
